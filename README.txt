Drupal Patches
Conditional fields patches is a merge of the following two patches:
https://www.drupal.org/files/issues/conditional_fields-1464950-310.patch
https://www.drupal.org/files/issues/conditional_fields-conditionally_required-1561272-139.patch
